<?php
namespace SDM\App\Core;
use SDM\App\Core\Config;
use SDM\App\Core\Auth;
/**
* Define the basics of SDM app
* Standard File
*/
class App
{

	public $theme;
	
	protected $viewDir;

	public $session;

	protected $params;

	protected $controller;

	protected $method;

	protected $publicDir;

	public $debug;

	protected $uri;

	protected $protocol;

	protected $database;

	protected $dbHandler;

	protected $twig;

	protected $auth;

	public $request;

	protected $namespace = "SDM\App\Main\\";

	public function __construct()
	{
		$this->publicDir = dirname(dirname(__DIR__))."/public";
		$this->viewDir = dirname(__DIR__)."/Views";
		$this->initConfig();
	}

	protected function initConfig()
	{
		$dotenv = new \Dotenv\Dotenv(dirname(dirname(__DIR__))."/public");
		$dotenv->load();
		$config = Config::init();
		$this->debug = $config['debug'];
		$this->theme = $config['theme'];
		$this->request = $_REQUEST;
		unset($this->request['path']);
		$this->database = $config['database'][$config['database']['in_use']];
		$this->database['in_use'] = $config['database']['in_use'];
		$loader = new \Twig_Loader_Filesystem($this->viewDir."/".$this->theme);
		$this->twig = new \Twig_Environment($loader,array(
    		'debug' => true,
		));
		$this->twig->addExtension(new \Twig_Extension_Debug());
		$this->setConfig();
		$this->auth = new Auth();
	}

	public function setConfig()
	{
		$this->setDebug();
		$uri = explode('/', $_SERVER['REQUEST_URI']);
		array_shift($uri);
		$this->uri = $uri;
		$this->setController();
		$this->setMethod();
		$this->setProtocol();
		$this->session = &$_SESSION;
	}

	public function setDebug()
	{
		if($this->debug)
		{
			error_reporting(E_ALL);
			ini_set('display_errors',1);
		}
	}

	public function setController()
	{
		if(empty(array_filter($this->uri)))
		{
			$this->controller = 'Home';
		}else{
			$this->controller = ucfirst($this->uri[0]);
		}
	}
		
	public function setMethod()
	{
		if(empty(array_filter($this->uri)))
		{
			$this->method = 'index';
		}else{
			if(strpos($this->uri[1], "?") === false )
			{
				$this->method = $this->uri[1];
			}else{
				$this->method = substr($this->uri[1], 0, strpos($this->uri[1], "?"));
				$this->params = substr($this->uri[1], strpos($this->uri[1], "?"));
			}
		}
	}

	public function setProtocol()
	{
		// To-do
	}

	public function call()
	{
		$this->controller =  $this->namespace . $this->controller;
		$method = (string) $this->method;
		$obj = new $this->controller();
		echo $obj->$method();
	}

	public function view($viewFile, $param=array())
	{
		$param['auth'] = false;
		extract($param);
		
		// Set alerts messages for views
		if(isset($this->session['alerts']))
		{
			$param['alerts'] = $this->session['alerts'];
		}

		// Set user information for views
		if($this->auth->userExists())
		{
			$param['auth'] = true;
			$param['user'] = $this->session['user'];
		}
		echo $this->twig->render($viewFile,$param);
	}
	
	public function url($path)
	{
		return $this->publicDir . "/" . $path;
	}
	
	public function redirect($params=array())
	{
		$host = $_SERVER['HTTP_HOST'];
		if(!empty($params))
		{
			$controller = $params[0];
			$view = $params[1];
		}else{
			$controller = 'Home';
			$view = 'index';
		}
		
		header("Location: http://$host/$controller/$view");
		exit;
	}


	public function setFlash($params)
	{
		$_SESSION['alerts'][$params[1]]=$params[0];
	}

	public function __destruct()
	{
		// remove alerst froom session
		unset($_SESSION['alerts']);
	}
}