<?php
namespace SDM\App\Core;
use SDM\App\Core\App;
/**
* Auth component for user/api authentication
* Standard File
*/
class Auth extends App
{
	private $token;

	public function __construct()
	{
		$this->token = null;
		$this->session = $_SESSION;
	}

	public function userExists(){
		if(isset($this->session['user']))
		{
			return true;
		}else{
			return false;
		}
	}

	public function authenticateApi($rememberToken){
		if($_SESSION['user']['rememberToken'] != $rememberToken)
		{
			
		}
	}

	public function checkTokenAccess(){
		// update $this->auth authenticateApi
	}

	public function checkAccess()
	{
		if(!$this->userExists() && $_SERVER['REQUEST_URI'] != '/Home/index')
		{
			$this->redirect(array('Home','index'));
		}
	}

}