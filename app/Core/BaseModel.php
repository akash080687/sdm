<?php
namespace SDM\App\Core;
use SDM\App\Core\App;
use SDM\App\Core\Database;
/**
* BaseModel class
* Standard File
*/
class BaseModel extends App
{
	protected $tableName;
	protected $primaryId;
	protected $orderBy;
	protected $dbHandler;

	function __construct()
	{
		parent::__construct();
	}
}