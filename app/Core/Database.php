<?php
namespace SDM\App\Core;
use SDM\App\Core\App;
/**
* Database wrapper
* Standard File
*/
class Database
{
	public static function connection($database)
	{
		if($database['driver'] == 'pgsql')
		{
			// Class connection
			try{
				$dbh = null;
				$dbh = new \PDO("pgsql:host=".$database['host'].";dbname=".$database['database'], $database['username'], $database['password']);
				$dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				if($dbh){
					return $dbh;
				}
				return false;
			}catch(\PDOException $e){
				$title = "Error";
				echo $error = $e->getMessage();exit;
			}	
		}else{
			// Other connections
		}
	}

}
