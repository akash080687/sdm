<?php
namespace  SDM\App\Main;
use SDM\App\Core\App;
use SDM\App\Core\Auth;
use SDM\App\Model\UserModel;
use SDM\App\Main\BatchManager;

class Api extends App
{
	protected $auth;
	private $authKey;
	private $batchToken;
	function __construct()
	{
		$this->auth = new Auth;
		$this->authKey = $_GET['authKey'];
		$this->batchToken = $_GET['batchToken'];
		if($this->validateAuth())
		{
			$this->initializeUser();
		}else{
			$response = array(
				"value" => array(),
				"msg" => "Not a valid user",
				"error" => 1
			);
			return json_encode();
		}
	}

	public function validateAuth()
	{
		return $this->auth->authenticateApi($this->authKey);
	}

	public function initializeUser()
	{
		$response = $this->userModel->getUserFromTokenId($this->authKey);
		if($response !== false)
		{
			$_SESSION['user'] = $response;
		}else{
			$response = array(
				"value" => array(),
				"msg" => "Not a valid user",
				"error" => 1
			);
			return json_encode();
		}
	}

	public function postBatchInfo()
	{
		
	}

	public function getBatchDetails()
	{
		
	}

	public function getBatchStatus()
	{

	}

	public function deleteBatch()
	{
		
	}
}