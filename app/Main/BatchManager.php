<?php
namespace SDM\App\Main;
use SDM\App\Core\App;
use SDM\App\Main\DataManager;
use SDM\App\Main\TokenManager;
use SDM\App\Main\ModelManager;
use SDM\App\Model\BatchModel;
use SDM\App\Main\LayerManager;

class BatchManager extends App
{
	protected $dataManager;
	protected $tokenManager;
	protected $modelManager;
	private $layerManager;
	function __construct()
	{
		parent::__construct();
		$this->dataManager = new DataManager;
		$this->tokenManager = new TokenManager;
		$this->modelManager = new ModelManager;
		$this->batchModel = new BatchModel;
		$this->layerManager = new LayerManager;
	}

	private function createBatch($data, $fileId, $tokenId, $supportingFiles)
	{
		$userId = $_SESSION['user']['user_id'];
		$level = ($data['advanced'] == 'false') ? 'beginner' : 'expert';
		$modelId = $data["model"];
		$projectName = $data["batch_name"];
		$bioclims = $data['bioclim'];
		$ouputPath = DataManager::userDir($projectName, $this->publicDir)."/result";
		$extent = 'null';
		
		$output = $this->batchModel->addBatch($userId,$tokenId, $fileId, $modelId, $ouputPath, $extent, $projectName, $bioclims, $level);
		if($output !== false)
		{
			if(!is_null($supportingFiles))
			{
				$this->dataManager->updateSupportingLayers($supportingFiles, $output);
			}
			$msg = ['SDM is submitted successfully. You can track the status at <b>"My Projects"</b> section.','success'];
			$this->setFlash($msg);
		}
	}

	public function saveBatch($data)
	{
		if(!empty($data))
		{
			$data['batch_name'] = preg_replace('/\s+/', '_', $data['batch_name']);
			$fileData = $_FILES['taxaFile'];
			$tokenId = $this->tokenManager->generateToken();
			$fileId = $this->dataManager->uploadFile($fileData, $data, false);
			$supportingLayers = array();
			$supportingFiles = null;
			if(!empty(array_filter($_FILES['supporting']['name'])))
			{
				$supportingLayers = $_FILES['supporting'];
				$supportingFiles = $this->dataManager->supportingFileUpload($supportingLayers, $data, true);
			}
			$this->createBatch($data, $fileId, $tokenId, $supportingFiles);
			$this->verifyDataStatus($tokenId);
			$this->redirect(array('Sdm','dashboard'));	
		}
		$this->setFlash(['Error in upload. Contact administrator','danger']);
	    return $this->redirect(array('Sdm','dashboard'));
	}

	public function verifyDataStatus($tokenId, $getStatus=null)
	{
		$dataStatus = $this->dataManager->verifyData($tokenId);
		if(is_bool($dataStatus) && $dataStatus == true)
		{
			$this->batchModel->changeStatus('In queue',$tokenId);
			$this->executeSdm($tokenId);
		}else{
			$this->batchModel->changeStatus('Error',$tokenId, $dataStatus);
		}
		if(!is_null($getStatus))
		{
			return $dataStatus;
		}
	}

	public function cleanandrun($projName)
	{
		$details = $this->batchModel->getBatchDetails($projName, 'projName');
		$tokenId = $details['token_id'];
		$dataStatus = $this->dataManager->cleanData($tokenId);
		$dataStatus = $this->verifyDataStatus($tokenId,1);
		if(is_bool($dataStatus) && $dataStatus == true)
		{
			echo true;
		}else{
			echo false;
		}
	}

	public function executeSdm($tokenId)
	{
		$getDetails = $this->batchModel->getBatchDetails($tokenId);
		$this->modelManager->modelRun($getDetails);
	}

	public function processStatus()
	{
		if(isset($_REQUEST['projName']))
		{
			$projName = trim($_REQUEST['projName']);
			$report = $this->batchModel->getStatusInPercentage($projName);
			if($report != 0)
			{
				echo json_encode($report);
			}else{
				echo false;
			}
		}
	}

	public function getProjects()
	{
		$userId = $_SESSION['user']['user_id'];
		return $this->batchModel->getBatchList($userId);
	}

	public function projectName($projectName)
	{
		return $this->batchModel->projectName($projectName);
	}

	public function errorStatus()
	{
		return $this->batchModel->errorStatus($_POST['projName']);	
	}

	public function deleteProject()
	{
		$layername = $_POST['layername'];
		$projName = $_POST['projName'];
		
		// Data delete
		// batch_master views sdm_output data_master token_master
		$this->batchModel->deleteProject($projName);
		// Layer delete
		$this->layerManager->deleteLayer($layername);
		// Folders delete
		try{
			$this->removeDir(DataManager::userDir($projName,$this->publicDir));	
		}catch(\Exception $e){
			$e->getMessage();exit;
		}
		
	}

	public function removeDir($dir) {
		foreach(scandir($dir) as $file) {
	        if ('.' === $file || '..' === $file) continue;
	        if (is_dir("$dir/$file")) $this->removeDir("$dir/$file");
	        else unlink("$dir/$file");
	    }
	    rmdir($dir);
	}

	public function getSdmLayers()
	{
		$userId = $_SESSION['user']['user_id'];
		return $this->batchModel->getSdmLayers($userId);
	}

	public function getUserClips()
	{
		return $this->batchModel->getUserClips();
	}

	public function clipExport($userName, $projFolder, $layerName, $file, $clipJobName)
	{
		$path = $this->publicDir."/sdm/$userName/$projFolder";
		$clipJobFilePath = $path."/clips/$clipJobName";
		mkdir($clipJobFilePath, 0777, true);
		$uploadedFile = $this->dataManager->clipFileRef($file, $clipJobFilePath);
		$zip = new \ZipArchive();
		$zip->open($uploadedFile);
		$zip->extractTo($clipJobFilePath);
		$uploadedFile = rtrim($uploadedFile,".zip");
		$fileToClip = glob("$path/result/outputs/shapefile/*.shp")[0];
		$scriptCommand = "$uploadedFile.shp $path/clips/$clipJobName $fileToClip";
		file_put_contents($clipJobFilePath."/clip.sh", $scriptCommand);
		file_put_contents($clipJobFilePath."/species.txt", pathinfo(basename($fileToClip), PATHINFO_FILENAME));
		$this->batchModel->trackClipReqeust($userName, $projFolder, $clipJobName, "/sdm/$userName/$projFolder/clips/$clipJobName/".pathinfo(basename($fileToClip), PATHINFO_FILENAME).'.zip');
		echo  json_encode(array('success'=>1,'path'=>$clipJobFilePath));
	}
}