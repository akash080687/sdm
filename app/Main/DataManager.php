<?php
namespace SDM\App\Main;
use SDM\App\Core\App;
use SDM\App\Main\Interfaces\DataInterface;
use SDM\App\Model\DataModel;

class DataManager extends App implements DataInterface
{
	private $dataModel;
	public function __construct()
	{
		parent::__construct();
		$this->dataModel =  new DataModel;
	}

	public function updateDataEntry($fileId, $data)
	{
		
	}

	public function uploadFile($postData, $data)
	{
		$userDir = $this->userDir($data['batch_name'], $this->publicDir);
		
		if ($postData['error'] == 0) {
	        $tmp_name = $postData["tmp_name"];
	        $name = basename($postData["name"]);
	        $name = str_replace(" ", "_", $name);
	        $uploadedFile = $userDir."/fileUploaded/$name";
	        try{
	        	move_uploaded_file($tmp_name, $uploadedFile);
	        	$dataId = $this->dataModel->addData($uploadedFile);
	        	return $dataId;
	        }catch(\Exception $e){
	        	echo $e;exit;
	        }
	    }else{
	    	$this->setFlash(['There is some issue with uploaded file. Please try again.','danger']);
	    	return $this->redirect(array('Sdm','dashboard'));
	    }
	    return false;
	}

	public function supportingFileUpload($postData, $data)
	{
		$userDir = $this->userDir($data['batch_name'], $this->publicDir);
		$cnt = count($postData['name']);
		$arr = array();
		for($i=0; $i<$cnt;$i++)
		{
			if ($postData['error'][$i] === 0) 
			{
		        $tmp_name = $postData["tmp_name"][$i];
		        $name = basename($postData["name"][$i]);
		        $name = str_replace(" ", "_", $name);
		        $uploadedFile = $userDir."/fileUploaded/supporting/$name";
		        try{
		        	move_uploaded_file($tmp_name, $uploadedFile);
		        	$dataId = $this->dataModel->addData($uploadedFile);
		        	$arr[] = $dataId;
		        }catch(\Exception $e){
		        	echo $e;exit;
		        }
		    }else{
		    	$this->setFlash(['There is some issue with uploaded file. Please try again.','danger']);
		    	return $this->redirect(array('Sdm','dashboard'));
		    }	
		}
		return $arr;
	}

	function deleteDataEntry($id)
	{
		
	}

	public static function userDir($projectName, $publicDir)
	{
		$user = str_replace(" ", "_", $_SESSION['user']['user_id']);
		$sdmDir = $publicDir."/sdm/";
		
		// Create user tmp folders
		if (!file_exists($sdmDir."$user/$projectName")) {
		    mkdir($sdmDir."$user/$projectName", 0777, true);
		    mkdir($sdmDir."$user/$projectName/fileUploaded", 0777, true);
		    mkdir($sdmDir."$user/$projectName/fileUploaded/supporting", 0777, true);
		    mkdir($sdmDir."$user/$projectName/clips", 0777, true);
		    mkdir($sdmDir."$user/$projectName/result", 0777, true);
		}
		
		$userDir = $sdmDir."$user/$projectName";
		return $userDir;
	}

	public function verifyData($tokenId)
	{
		return $this->dataModel->verifyData($tokenId);
	}

	public function cleanData($tokenId)
	{
		return $this->dataModel->cleanData($tokenId);
	}

	public function updateSupportingLayers($ids, $id)
	{
		return $this->dataModel->updateSupportingLayers($ids, $id);	
	}

	public function clipFileRef($postData, $uploadFilePath)
	{
		if ($postData['error'] === 0) 
		{
	        $tmp_name = $postData["tmp_name"];
	        $name = basename($postData["name"]);
	        $name = str_replace(" ", "_", $name);
	        $uploadedFile = $uploadFilePath."/$name";
	        try{
	        	move_uploaded_file($tmp_name, $uploadedFile);
	        	return $uploadedFile;
	        }catch(\Exception $e){
	        	echo $e;exit;
	        }
	    }else{
	    	$this->setFlash(['There is some issue with uploaded file. Please try again.','danger']);
	    	return $this->redirect(array('Sdm','dashboard'));
	    }	
	}
}