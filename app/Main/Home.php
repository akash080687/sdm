<?php
namespace SDM\App\Main;
use SDM\App\Core\App;
/**
* Home Controller
*/
class Home extends App
{
	protected $auth;
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$title = 'Homepage';
		$projectCount = "";
		$likesCount = "";
		$users = "";
		if($this->auth->userExists())
		{
			$this->redirect(array('Sdm','dashboard'));
		}
		return $this->view('index.html',compact('title'));
	}
	
	public function sdmHome()
	{
		if($this->auth->userExists())
		{
			$title = "Home";
			return $this->view('sdmHome.html');
		} else{
			// prompt login
			$this->redirect(array('User','login'));
		}
	}

	public function about()
	{
		$title = "About";
		$this->view('about.html',compact('title'));	
	}
}