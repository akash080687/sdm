<?php
namespace SDM\App\Main\Interfaces;

interface DataInterface
{
	function uploadFile($postData, $data);
	function deleteDataEntry($id);
	function updateDataEntry($fileId, $data);
	static function userDir($projectName, $publicDir);
}