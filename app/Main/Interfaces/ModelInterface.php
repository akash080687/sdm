<?php
namespace SDM\App\Main\Interfaces;

interface ModelInterface
{
	function getBatchId();
	function runModel();
	function checkStatus();
	function detailLogs();
	function getReport();
	function setParameters();
	function getParamteresList();
}