<?php
namespace SDM\App\Main;
use SDM\App\Core\App;
use SDM\App\Model\LayerModel;
/**
* Layer Manager
*/
class LayerManager extends App
{
	private $curl;
	private $layerModel;
	
	function __construct()
	{
		parent::__construct();
		$this->curl = curl_init();
		$this->layerModel = new LayerModel;
	}

	public function deleteLayer($layerName)
	{
		if($this->checkLayers($layerName))
		{
			try{
				$layerUrl = "http://127.0.0.1:8080/geoserver/rest/layers/".$layerName.".json";
				curl_setopt($this->curl, CURLOPT_URL , $layerUrl);
			    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
				curl_setopt($this->curl, CURLOPT_USERPWD, "admin:geoserver");
				$result = curl_exec($this->curl);

				$storeUrl = "http://127.0.0.1:8080/geoserver/rest/workspaces/sdm/datastores/".$layerName.".xml";
				curl_setopt($this->curl, CURLOPT_URL , $storeUrl);
			    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE");
				curl_setopt($this->curl, CURLOPT_USERPWD, "admin:geoserver");
				$result = curl_exec($this->curl);
			}catch(\Exception $e){
				echo $e->getMessage();exit;
			}
		}
	}

	public function checkLayers($layerName)
	{
		$url = "http://127.0.0.1:8080/geoserver/rest/layers";
		curl_setopt($this->curl, CURLOPT_URL , $url);
	    curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($this->curl, CURLOPT_USERPWD, "admin:geoserver");
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($this->curl);
		$layers = json_decode($result);
		if($layers != null)
		{
			foreach ($layers->layers->layer as $value) {
			if($value->name == $layerName)
				{
					return true;
				}
			}	
			return false;
		}
		return true;
	}

	public function getCategories()
	{
		return $this->layerModel->getCategories();
	}

	public function getSubCategories()
	{
		$catId = $_POST['catId'];
		$result = $this->layerModel->getSubCategories($catId);
		$html = "<ul class='list-group subcatlist'>";
		foreach ($result as $key => $value) {
			$html .= "<li class='list-group-item' data-attr='".$value['sc_id']."'>".$value['sc_name']."</li>";
		}
		$html .= "</ul>";
		echo $html;
	}

	public function getLayerList()
	{
		$subCatId = $_POST['subCatId'];
		$result = $this->layerModel->getLayerList($subCatId);
		$html = "<ul class='list-group'>";
		foreach ($result as $key => $value) {
			$html .= "<li class='list-group-item' data-layer='".$value['layer_name']."' data-workspace='".$value['name']."' data-store='".$value['name']."' data-temporal='".$value['is_temporal']."'>".$value['layer_displayname'].
				"<i class='fa fa-eye fa-md viewLayer float-md-right'></i>
				</li>";
		}
		// <i class='fa fa-eye-slash fa-md dismissLayer float-md-right'></i>
		$html .= "</ul>";
		echo $html;
	}

	public function getSdmLayers()
	{
		
	}
}