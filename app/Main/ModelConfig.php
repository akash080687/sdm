<?php
namespace SDM\App\Main;
/**
* Model realted Config class
* Standard File
*/
class ModelConfig
{

    protected $smdScriptHome;

    protected $bioClimlayerFolderPath;

    protected $otherLayersFolderPath;

    protected $geoDataDir;

    protected $credentials;

    protected $geoserverIpAddress;

    public function __construct()
    {
        $this->smdScriptHome = $_ENV['SDM'];
        $this->bioClimlayerFolderPath = $_ENV['BIO'];
        $this->otherLayersFolderPath = "";
        $this->geoDataDir="/opt/tomcat/webapps/geoserver/data/data";
        $this->credentials=$_ENV['GEOCRED'];
        $this->geoserverIpAddress=$_ENV['GEOSERV'];
    }

    public function getModelParams($model)
    {
        switch ($model) {
            case 'maxent':
                return [
                    'maxentDir' => $this->smdScriptHome."/maxent",
                    'environmentallayers' => $this->bioClimlayerFolderPath,
                    'geoDataDir' => $this->geoDataDir,
                    'credentials' => $this->credentials,
                    'ipAddress' => $this->geoserverIpAddress,
                ];
                break;
            
            default:
                return false;
                break;
        }
    }

    public function getBaseUrl()
    {
        return $_ENV['BASEURL'];
    }
}

