<?php
namespace SDM\App\Main;
use SDM\App\Core\App;
use SDM\App\Main\ModelConfig;
use SDM\App\Model\ModelModel;
use SDM\App\Model\DataModel;
use SDM\App\Model\TokenModel;
use SDM\App\Model\BatchModel;

class ModelManager extends App
{
	protected $modelConfig;
	protected $modelModel;
	protected $dataModel;
	protected $tokenModel;
	protected $batchModel;
	protected $parameters;

	public function __construct()
	{
		$this->modelConfig = new ModelConfig;
		$this->modelModel = new ModelModel;
		$this->dataModel = new DataModel;
		$this->tokenModel = new TokenModel;
		$this->batchModel = new BatchModel;
	}

	public function modelRun($sdmInfo)
	{
		$modelName = $this->modelModel->getModelName($sdmInfo['m_id']);
		$this->parameters = $this->modelConfig->getModelParams($modelName);
		$executeString = $this->generateExecuteScript($sdmInfo, $modelName);
	}

	private function generateExecuteScript($sdmInfo, $modelName)
	{
		$executeString = "";
		// Based on modelName
		switch ($modelName) {
			case 'maxent':
				$modelName = $this->modelModel->getModelName($sdmInfo['m_id']);
				$csvFile = $this->dataModel->getDataPath($sdmInfo['d_id']);
				$outputPath = $sdmInfo['output_path']; 
				$logPath = $sdmInfo['output_path'];
				$sdmDir = $this->parameters['maxentDir'];
				$bioclimDir = $this->parameters['environmentallayers'];
				$geoDataDir = $this->parameters['geoDataDir'];
				$ipAddress = $this->parameters['ipAddress'];
				$credentials = $this->parameters['credentials']; 
				$batchId = $sdmInfo['b_id'];
				$climLayers = $sdmInfo['env_variables'];
				$level = $sdmInfo['level'];
				$executeString = $sdmDir."/".strtolower($modelName)."_process.sh  $csvFile $outputPath $logPath $sdmDir $bioclimDir $geoDataDir $ipAddress $credentials $batchId $climLayers $level";
				break;
			default:
				break;
		}
		
		$this->batchModel->updateScriptCommand($sdmInfo['token_id'], $executeString);
	}
}