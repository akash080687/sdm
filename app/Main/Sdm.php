<?php
namespace SDM\App\Main;
use SDM\App\Core\App;
use SDM\App\Main\BatchManager;
use SDM\App\Main\LayerManager;
use SDM\App\Model\IbisBatchModel;
/**
* SDM class contains access to Database, system api and dashboards
*/
class Sdm extends App
{
	private $batchManager;
	private $layerManager;
	public function __construct()
	{
		parent::__construct();
		$this->auth->checkAccess();
		$this->batchManager = new BatchManager();
		$this->layerManager = new LayerManager;
		$this->ibisBatchModel = new IbisBatchModel;
	}

	public function dashboard()
	{
		$title = "Dashboard";
		$projects = $this->batchManager->getProjects();
		$username = $this->session['user']['name'];
		$userId = $this->session['user']['user_id'];
		// Supporting layers
		$categories = $this->layerManager->getCategories();
		$userClips = $this->batchManager->getUserClips();
		// SDM layers
		$sdmLayers = $this->batchManager->getSdmLayers();
		return $this->view('sdmHome.html',compact('title','projects','categories','username', 'sdmLayers','userId','userClips'));
	}

	public function runSdm()
	{
		$this->request['bioclim'] = isset($this->request['bioclim']) ? implode(',',$this->request['bioclim']) : implode(',', range(1, 19));
		$this->batchManager->saveBatch($this->request);
	}

	public function projectNameExists()
	{
		$name = $_POST['name'];
		return $this->batchManager->projectName($name);
	}

	public function rerun()
	{
		$project = $_POST['project'];
		return $this->batchManager->cleanandrun($project);
	}

	public function createNewProject()
	{
		$title = "Run new SDM";
		return $this->view('addNewProject.html',compact('title'));
	}

	public function clipExport()
	{
		$file = $_FILES['clipFile'];
		list($userName, $projFolder, $layerName) = explode(":", $_POST['sdmLayer']);
		$clipJobName = $_POST['clipJob'];
		$this->batchManager->clipExport($userName, $projFolder, $layerName, $file, $clipJobName);
	}

	public function checkClipDone()
	{
		$url = $_POST['url'];
		if(file_exists($url."/completed.txt"))
		{
			return true;
		}else{
			return false;
		}
	}

	public function ibis()
	{
		$title = "Monitor IBIS Status";
		$batchDetails = $this->ibisBatchModel->getDetails();
		$totalSpecies = count(array_map('str_getcsv', file($this->publicDir."/birds.csv"))[0]);
		$completed = $this->ibisBatchModel->completed();
		$statusDetails = $this->ibisBatchModel->status();
		$status = $statusDetails != null ? 'IBIS - Running' : 'Not Running';
		$source = $statusDetails != null ? $statusDetails['source'] : '-';
		return $this->view('monitor.html',compact('title','batchDetails','totalSpecies','completed','status'));
	}

	public function findIbis()
	{
		$sdmForSpecies = $this->ibisBatchModel->findSpecies();
		if($sdmForSpecies != null)
		{
			$layerListIbis = array();
			foreach ($sdmForSpecies as $value) 
			{
				$mainK = $value['project_name']." - [".$value['source']."]";
				$layerListIbis[$mainK] = $value['layer_name'];
			}
			return json_encode($layerListIbis);
		}else{
			return json_encode(array('No data found.'));
		}
	}
}