<?php
namespace SDM\App\Main;
use SDM\App\Model\TokenModel;
/**
* 
*/
class TokenManager
{

	private $tokenModel;

	public function __construct()
	{
		$this->tokenModel = new TokenModel;
	}

	public function checkTokenStatus()
	{
		// To do
	}

	public function generateToken()
	{
		$token = \IcyApril\CryptoLib::randomHex(25);
		$userId = $_SESSION['user']['user_id'];
		$refId = 'self';
		$tokenId = $this->tokenModel->addToken($token, $userId, $refId);
		return $tokenId;
	}

	public function disableToken()
	{
		// To do
	}

	public function getTokenFromId($id)
	{
		$this->tokenModel->getTokenfromId($id);
	}
}