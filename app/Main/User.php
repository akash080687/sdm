<?php
namespace SDM\App\Main;
use SDM\App\Core\App;
use SDM\App\Core\Auth;
use SDM\App\Model\UserModel;
/**
* Home Controller
*/
class User extends App
{
	protected $model;
	protected $query;

	public function __construct()
	{
		parent::__construct();
		$this->model = new UserModel($this->dbHandler);
	}

	public function login(){
		$title = "Login";
		if(!empty($this->request))
		{
			$response = $this->model->getUser($this->request);
			extract($response);
			if($status)
			{
				$this->setFlash($msg);
				$this->redirect(array('Sdm','dashboard'));
			}else{
				$this->setFlash($msg);
				$this->view('login.html',compact('title'));
			}
		}else{
			$this->view('login.html',compact('title'));
		}
	}

	public function logout(){
		session_destroy();
		$this->setFlash(['You have successfully logged out.','success']);
		$this->redirect(array('Home','index'));
	}

	public function register(){
		if(!empty($this->request))
		{
			$msg = $this->model->setUser($this->request);
			$this->setFlash($msg);
		}
		$title = "Register";
		$this->view('register.html',compact('title'));
	}

	public function account()
	{
		$title = "My Account";
		$userDetails = $this->model->getUserFromId($_SESSION['user']['user_id']);
		if(!empty($this->request) && $userDetails)
		{
			$msg = $this->model->updateUser($userDetails['user_id'], $this->request);
			$this->setFlash($msg['msg']);
			$this->redirect(array('User','account'));
		}else{
			$this->view('account.html',compact('title','userDetails'));	
		}
	}
}