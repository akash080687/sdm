<?php
namespace SDM\App\Model;
use SDM\App\Core\BaseModel;
use SDM\App\Core\Database;
/**
* Batch Model
*/
class BatchModel extends BaseModel
{
	function __construct()
	{
		parent::__construct();
		$this->tableName = 'batch_master';
	}

	public function addBatch($userId, $tokenId, $fileId, $modelId, $ouputPath, $extent, $projectName, $bioClims, $level)
	{
		$status = "New";
		$this->dbHandler = Database::connection($this->database);
		$stat = $this->dbHandler->prepare("insert into ".$this->tableName." (user_id, status, token_id, d_id, m_id, output_path, extent, project_name, env_variables, level) values(?,?,?,?,?,?,?,?,?,?)");
		$stat->bindParam(1,$userId,\PDO::PARAM_STR);
		// Default
		$stat->bindParam(2,$status,\PDO::PARAM_STR);
		$stat->bindParam(3,$tokenId,\PDO::PARAM_STR);
		$stat->bindParam(4,$fileId,\PDO::PARAM_STR);
		$stat->bindParam(5,$modelId,\PDO::PARAM_STR);
		$stat->bindParam(6,$ouputPath,\PDO::PARAM_STR);
		$stat->bindParam(7,$extent,\PDO::PARAM_STR);
		$stat->bindParam(8,$projectName,\PDO::PARAM_STR);
		$stat->bindParam(9,$bioClims,\PDO::PARAM_STR);
		$stat->bindParam(10,$level, \PDO::PARAM_STR);
		try{
    		$stat->execute();
    		// $this->dbHandler->commit();
    		// logger mono
    		$result =  $this->dbHandler->lastInsertId();
    	} catch(\PDOException $e) {
    		echo "From addBatch ".$e->getMessage();exit;
    		// logger mono
    		// $msg = [$sql . "<br>" . $e->getMessage(),'danger'];
    		$result = false;
    	}	
		$this->dbHandler = null;
		return $result;
	}

	public function getBatchDetails($identifier,$type="token")
	{
		switch ($type) {
			case 'token':
				$condition = "token_id = '$identifier'";
				break;
			case 'projName':
				$condition = "project_name = '$identifier'";
				break;
			default:
				$condition = "token_id = '$identifier'";
				break;
		}
		
		$this->dbHandler = Database::connection($this->database);
		$sql = "SELECT * from ".$this->tableName." where ".$condition;
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			$result = !empty(array_filter($result)) ? $result[0] : "";
		}catch(\PDOException $e){
			$error = $e->getMessage();
			// debug 
			echo "From getBatchDetails - ". $error;exit;
		}
		$this->dbHandler = null;
		return $result;
	}

	public function changeStatus($status,$tokenId, $message = null)
	{
		$this->dbHandler = Database::connection($this->database);
		
		if(is_null($message))
		{
			$sql = "UPDATE ".$this->tableName." set status = '".$status. "' where token_id=".$tokenId;	
		}else{
			$sql = "UPDATE ".$this->tableName." set status = '".$status. "', description = '".$message."' where token_id=".$tokenId;
		}
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo "From changeStatus - ". $error;exit;
		}
		$this->dbHandler = null;
	}

	public function updateScriptCommand($tokenId, $command)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("update ".$this->tableName." set scriptcommand = ? where token_id = ?");
		$statement->bindParam(1,$command, \PDO::PARAM_STR);
		$statement->bindParam(2,$tokenId, \PDO::PARAM_STR);
		try{
			$statement->execute();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;
		}
		$this->dbHandler = null;
	}

	public function getBatchList($userId)
	{
		$this->dbHandler = Database::connection($this->database);
		$stmt = $this->dbHandler->prepare("select project_name, status, layer_name from ".$this->tableName." where user_id  = ?");
		$stmt->bindParam(1,$userId, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;
		}
		return $result;
		$this->dbHandler = null;	
	}

	public function projectName($name)
	{
		$this->dbHandler = Database::connection($this->database);
		$stmt = $this->dbHandler->prepare("select count(*) from ".$this->tableName." where project_name  = ?");
		$stmt->bindParam(1,$name, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			$result = !empty(array_filter($result)) ? $result[0]['count'] : 0;
		} catch (\PDOException $e) {
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;	
		}
		return $result;
	}

	public function getStatusInPercentage($projName)
	{
		// alter table batch_master add column process varchar(20);
		$this->dbHandler = Database::connection($this->database);
		$stmt = $this->dbHandler->prepare("select status, process from ".$this->tableName." where project_name  = ?");
		$stmt->bindParam(1,$projName, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			$result = !empty(array_filter($result)) && ! is_null($result[0]['process']) ? $result[0] : 0;
		} catch (\PDOException $e) {
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;	
		}
		return $result;
	}

	public function errorStatus($projName)
	{
		$this->dbHandler = Database::connection($this->database);
		$stmt = $this->dbHandler->prepare("select description from ".$this->tableName." where project_name  = ?");
		$stmt->bindParam(1,$projName, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			$result = !empty(array_filter($result)) && ! is_null($result[0]['description']) ? $result[0]['description'] : 0;
		} catch (\PDOException $e) {
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;	
		}
		echo $result;
	}

	public function deleteProject($projName)
	{
		$getDetails = $this->getBatchDetails($projName, 'projName');
		$this->dbHandler = Database::connection($this->database);
		$stmtBatch = $this->dbHandler->prepare("delete from batch_master where project_name  = ?");
		$stmtData = $this->dbHandler->prepare("delete from data_master where d_id  = ?");
		$stmtSdmOutput = $this->dbHandler->prepare("delete from sdm_output where batch_id  = ?");
		$stmtToken = $this->dbHandler->prepare("delete from token_master where token_id  = ?");
		
		$stmtBatch->bindParam(1,$getDetails['project_name'], \PDO::PARAM_STR);
		$stmtData->bindParam(1,$getDetails['d_id'], \PDO::PARAM_STR);
		$stmtSdmOutput->bindParam(1,$getDetails['b_id'], \PDO::PARAM_STR);
		$stmtToken->bindParam(1,$getDetails['token_id'], \PDO::PARAM_STR);
		
		try
		{
			$stmtBatch->execute();
			$stmtData->execute();
			$stmtSdmOutput->execute();
			$stmtToken->execute();
			return true;
		}catch(\PDOException $e)
		{
			$error = $e->getMessage();
			echo "From deleteProject BatchModel : ".$error;exit;
		}
		return false;
	}

	public function getSdmLayers($userId)
	{
		$this->dbHandler = Database::connection($this->database);
		$stmt = $this->dbHandler->prepare("select project_name,layer_name from ".$this->tableName." where user_id  = ? and process = 100");
		$stmt->bindParam(1,$userId, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;
		}
		return $result;
		$this->dbHandler = null;	
	}

	public function getUserClips()
	{
		$userId = $_SESSION['user']['user_id'];
		$this->dbHandler = Database::connection($this->database);
		$stmt = $this->dbHandler->prepare("select * from user_clips where user_id  = ?");
		$stmt->bindParam(1,$userId, \PDO::PARAM_STR);
		try{
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;
		}
		return $result;
		$this->dbHandler = null;
	}

	public function trackClipReqeust($userName, $projFolder, $clipJobName, $path)
	{
		$this->dbHandler = Database::connection($this->database);
		$stat = $this->dbHandler->prepare("insert into user_clips (user_id, project_name, job_name, path) values(?,?,?,?)");
		$stat->bindParam(1,$userName,\PDO::PARAM_STR);
		$stat->bindParam(2,$projFolder,\PDO::PARAM_STR);
		$stat->bindParam(3,$clipJobName,\PDO::PARAM_STR);
		$stat->bindParam(4,$path,\PDO::PARAM_STR);
		try{
			$stat->execute();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo "From  updateScriptCommand - ". $error;exit;
		}
		return true;
		$this->dbHandler = null;
	}
}