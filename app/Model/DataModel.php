<?php
namespace SDM\App\Model;
use SDM\App\Core\BaseModel;
use SDM\App\Core\Database;
use SDM\App\Model\BatchModel;
/**
* Data Model
*/
class DataModel extends BaseModel
{
	private $dataParsed;
	private $bounds;
	private $error;
	function __construct()
	{
		parent::__construct();
		$this->tableName = 'data_master';
		$this->batchModel = new BatchModel;
		$this->bounds['a'] = ['x'=>65,'y'=>40];
		$this->bounds['b'] = ['x'=>100,'y'=>5];
	}

	public function addData($path)
	{
		$this->dbHandler = Database::connection($this->database);
		$stat = $this->dbHandler->prepare("insert into ".$this->tableName." (d_path) values(?)");
		$stat->bindParam(1,$path,\PDO::PARAM_STR);
		try{
    		$stat->execute();
    		$result =  $this->dbHandler->lastInsertId();
    	} catch(\PDOException $e) {
    		echo $e->getMessage();exit;
    		$result = false;
    	}	
		$this->dbHandler = null;
		return $result;
	}

	/**
	* Return type boolean
	*/
	public function verifyData($tokenId)
	{
		$batchDetails = $this->batchModel->getBatchDetails($tokenId);
		$filePath = $this->getDataPath($batchDetails['d_id']);
		$this->readData($filePath, $tokenId);
		$val = $this->dataError($tokenId);
		return $val;
	}

	public function getDataPath($dataId)
	{
		$condition = "d_id = '$dataId'";
		$this->dbHandler = Database::connection($this->database);
		$sql = "SELECT d_path from ".$this->tableName." where ".$condition;
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			// debug 
			echo "From getDataPath - ". $error;exit;
		}
		return $result[0]['d_path'];
	}

	public function readData($filePath)
	{
		if( ($handler=fopen($filePath, "r")) !== false)
		{
			$cnt = 0;
			while(($csvData = fgetcsv($handler, 1000, ",")) !== false)
			{
				$cnt++;
				if($cnt!=1) // skip the headers line
				{
					if($this->checkData($csvData))
					{
						break;
					}
				}
			}
		}else{
			$this->error = "File : unable to read.";	
		}
	}

	public function checkData($data)
	{
		if(!
			(
				($this->bounds['a']['x'] < floatval($data['1']) && $this->bounds['a']['y'] > floatval($data['2'])) &&
				($this->bounds['b']['x'] > floatval($data['1']) && $this->bounds['b']['y'] < floatval($data['2']))
			)
		){
			$this->error="Bounds : Data out of allowed bounds, Allowed range is within points : [65,40] -[100,5]";
			return true;
		}
		return false;
	}

	public function dataError($tokenId)
	{
		if(is_null($this->error))
		{
			return true;
		}else{
			return $this->error;
		}
	}


	public function cleanData($tokenId)
	{
		$batchDetails = $this->batchModel->getBatchDetails($tokenId);
		$filePath = $this->getDataPath($batchDetails['d_id']);
		$this->updateCleanData($filePath, $tokenId);
		$val = $this->dataError($tokenId);
		return $val;
	}


	public function updateCleanData($filePath)
	{
		$cleanCsv = "";
		if( ($handler=fopen($filePath, "r")) !== false)
		{
			$cnt = 0;
			while(($csvData = fgetcsv($handler, 1000, ",")) !== false)
			{
				$cnt++;
				if($cnt!=1)
				{
					if(!$this->checkData($csvData))
					{
						$cleanCsv[] = $csvData;	
					}
				}
			}
		}else{
			$this->error = "File : unable to read.";	
		}
		$f = fopen($filePath, "w");
		foreach ($cleanCsv as $value) {
			fputcsv($f, $value);
		}
	}

	public function updateSupportingLayers($ids, $batchId)
	{
		$this->dbHandler = Database::connection($this->database);
		$condition = ' d_id in ('.implode(",", $ids).')';
		$sql = "update  ".$this->tableName." set cust_batch_id = ? where ".$condition;
		$stat = $this->dbHandler->prepare($sql);
		$stat->bindParam(1,$batchId,\PDO::PARAM_INT);
		try{
			$stat->execute();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			echo "From getDataPath - ". $error;exit;
		}
		return true;
	}
}