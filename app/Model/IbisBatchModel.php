<?php
namespace SDM\App\Model;
use SDM\App\Core\BaseModel;
use SDM\App\Core\Database;
/**
* Batch Model
*/
class IbisBatchModel extends BaseModel
{
	private $config;
	function __construct()
	{
		parent::__construct();
		$this->tableName = 'ibis_batch_master';
		$this->config = \SDM\App\Core\Config::init();
	}

	public function getDetails()
	{
		$ibis=$this->config['database']['ibis'];
		$this->dbHandler = Database::connection($ibis);
		$sql = "select project_name, process, status, source from ".$this->tableName." order by id desc";
		try {
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		} catch (Exception $e) {
			echo "getCategories error ".$e->getMessage();exit;
		}
		return $result;
	}

	public function completed()
	{
		$ibis=$this->config['database']['ibis'];
		$this->dbHandler = Database::connection($ibis);
		$sql = "select count(distinct(project_name)) as cnt from ".$this->tableName." where process in ('-1','100') ";
		try {
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		} catch (Exception $e) {
			echo "getCategories error ".$e->getMessage();exit;
		}
		return $result[0]['cnt'];
	}

	public function status()
	{
		$ibis=$this->config['database']['ibis'];
		$this->dbHandler = Database::connection($ibis);
		$sql = "select source from ".$this->tableName." where status='Running'";
		try {
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		} catch (Exception $e) {
			echo "getCategories error ".$e->getMessage();exit;
		}
		return isset($result[0]) ? $result[0] : null;
	}

	public function findSpecies()
	{
		$ibis=$this->config['database']['ibis'];
		$this->dbHandler = Database::connection($ibis);
		$sql = "select layer_name, source, project_name from $this->tableName where status='Completed'";
		try {
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		} catch (Exception $e) {
			echo "getCategories error ".$e->getMessage();exit;
		}
		return isset($result[0]) ? $result : null;
	}
}
