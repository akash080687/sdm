<?php
namespace SDM\App\Model;
use SDM\App\Core\BaseModel;
use SDM\App\Core\Database;
/**
* 
*/
class LayerModel extends BaseModel
{
	private $category;
	private $subCategory;
	private $layerwWorkspace;
	private $layerStore;

	function __construct()
	{
		parent::__construct();
		$this->tableName = 'layer_info';
		$this->category = 'layer_category';
		$this->subCategory = 'layer_sub_category';
		$this->layerwWorkspace = 'layer_workspace';
		$this->layerStore = 'layer_store';
	}

	public function getCategories()
	{
		$this->dbHandler = Database::connection($this->database);
		$sql = "select c_id,c_name from ".$this->category." where visible=1";
		try {
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		} catch (Exception $e) {
			echo "getCategories error ".$e->getMessage();exit;
		}
		return $result;
	}

	public function getSubCategories($catId)
	{
		$this->dbHandler = Database::connection($this->database);
		$sql = "select sc_id, sc_name from ".$this->subCategory." where c_id=".$catId;
		try {
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		} catch (Exception $e) {
			echo "getSubCategories error ".$e->getMessage();exit;
		}
		return $result;	
	}

	public function getLayerList($subCatId)
	{
		$this->dbHandler = Database::connection($this->database);
		$sql = "select li.layer_name, li.layer_displayname, w.name, s.store_name, li.is_temporal from ".$this->tableName." as li, ".$this->layerwWorkspace." as w, ".$this->layerStore." as s  where sc_id=".$subCatId." and li.w_id=w.w_id and li.s_id = s.s_id";
		try {
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		} catch (Exception $e) {
			echo "getSubCategories error ".$e->getMessage();exit;
		}
		return $result;	
	}

	public function getFullList()
	{
		$this->dbHandler = Database::connection($this->database);
		$sql = "select c.c_name, sc.sc_name, li.layer_displayname, li.layer_name, w.name, s.store_name, li.is_temporal from ".
				$this->category." as c, ".
				$this->subCategory." as sc, ".
				$this->layerStore." as s, ".
				$this->layerwWorkspace." as w, ".
				$this->tableName." as li ".
				"where c.c_id = sc.c_id and sc.sc_id = li.sc_id and w.w_id=li.w_id and s.s_id = li.s_id";
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
			$result = count($result) == 0 ? null : $result;
		}catch(\PDOException $e){
			$error = $e->getMessage();
			// debug 
			echo "From getDataPath - ". $error;exit;
		}
		return $result;
	}
}