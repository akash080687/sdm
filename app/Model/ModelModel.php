<?php
namespace SDM\App\Model;

use SDM\App\Core\BaseModel;
use SDM\App\Core\Database;
/**
* 
*/
class ModelModel extends BaseModel
{
	
	function __construct()
	{
		parent::__construct();
		$this->tableName = 'model_master';
	}

	public function getModelName($modelId)
	{
		$condition = "m_id = '$modelId'";
		$this->dbHandler = Database::connection($this->database);
		$sql = "SELECT name from ".$this->tableName." where ".$condition;
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			// debug 
			echo "From getModelName - ". $error;exit;
		}
		return strtolower($result[0]['name']);
	}
}