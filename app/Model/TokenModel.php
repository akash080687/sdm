<?php
namespace SDM\App\Model;
use SDM\App\Core\BaseModel;
use SDM\App\Core\Database;
use IcyApril\Cryptolib;
/**
* Data Model
*/
class TokenModel extends BaseModel
{
	
	function __construct()
	{
		parent::__construct();
		$this->tableName = 'token_master';
	}

	public function addToken($path,$userId="self")
	{
		$this->dbHandler = Database::connection($this->database);
		
		$userId = isset($_SESSION['user']['user_id']) ? $_SESSION['user']['user_id'] : 0;
		
		$stat = $this->dbHandler->prepare("insert into ".$this->tableName." (token_id,user_id,ref_id) values(?,?,?)");
		
		$tokenId = Cryptolib::randomHex(100);
		
		$stat->bindParam(1,$tokenId,\PDO::PARAM_STR);
		$stat->bindParam(2,$userId,\PDO::PARAM_STR);
		$stat->bindParam(3,$refId,\PDO::PARAM_STR);
		
		try{
    		$stat->execute();
    		// $this->dbHandler->commit();
    		// logger mono
    		$result =  $this->dbHandler->lastInsertId();
    	} catch(\PDOException $e) {
    		echo $e->getMessage();exit;
    	}	
		$this->dbHandler = null;
		return $result;
	}

	public function getTokenfromId($id)
	{
		$condition = "srno = '$id'";
		$this->dbHandler = Database::connection($this->database);
		$sql = "SELECT token_id from ".$this->tableName." where ".$condition;
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
			// debug 
			echo "From getTokenfromId - ". $error;exit;
		}
		return $result[0]['token_id'];
	}
}