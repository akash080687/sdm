<?php
namespace SDM\App\Model;
use SDM\App\Core\BaseModel;
use SDM\App\Core\Database;
use IcyApril\CryptoLib;
/**
* User Model 
* Standard File
*/
class UserModel extends BaseModel
{
	public function __construct()
	{
		parent::__construct();
		$this->tableName = 'user_master';
	}

	public function setUser($data)
	{
		$randomHex = CryptoLib::randomHex(40);
		$this->dbHandler = Database::connection($this->database);
		if($this->checkEmailExists($data['email']))
		{
			$statement = $this->dbHandler->prepare("insert into ".$this->tableName." (name,email,address,password,mob_number, remember_token) VALUES (?,?,?,?,?,?)");
			$statement->bindParam(1,$data['name'], \PDO::PARAM_STR);
			$statement->bindParam(2, $data['email'], \PDO::PARAM_STR);
			$statement->bindParam(3, $data['address'], \PDO::PARAM_STR);
			$statement->bindParam(4, $data['password'], \PDO::PARAM_STR);
			$statement->bindParam(5, $data['mob_number'], \PDO::PARAM_STR);
			$statement->bindParam(6, $randomHex, \PDO::PARAM_STR);
			try{
	    		$statement->execute();
	    		// $this->dbHandler->commit();
	    		$msg = ['You have been registered successfully.','success'];
	    	} catch(\PDOException $e) {
	    		$msg = [$sql . "<br>" . $e->getMessage(),'danger'];
	    	}	
		}else{
			$msg = ['The email id you provided already exists.','danger'];
		}
		$this->dbHandler = null;
		return $msg;
	}

	public function checkEmailExists($email, $info=false)
	{
		$condition = "email = '$email'";
		$sql = "SELECT * from ".$this->tableName." where ".$condition;
		try{
			$stmt = $this->dbHandler->prepare($sql);
			$stmt->execute();
			$stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $stmt->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		
		if($info)
		{
			return $result;
		} else {
			return (count($result) == 0 ? true : false);
		}
		
	}

	public function checkCredentials($data, $info=false)
	{
		$statement = $this->dbHandler->prepare("select * from ".$this->tableName." where email = ? and password = ?");
		$statement->bindParam(1,$data['email'], \PDO::PARAM_STR);
		$statement->bindParam(2, $data['password'], \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		
		if($info)
		{
			return $result;
		} else {
			return (count($result) == 0 ? true : false);
		}
		
	}

	public function getUser($data)
	{
		$this->dbHandler = Database::connection($this->database);
		$user = $this->checkCredentials($data,true);
		if(count($user) != 0)
		{
			$status = true;
			$_SESSION['user'] = $user[0];
			$msg = ["Logged in successfully.",'success'];
		}else{
			$status = false;
			$msg = ["Error in logging. Please verify the credentials provided.",'danger'];
		}
		$this->dbHandler = null;
		return compact('msg','status');
	}

	public function getUserFromId($userId)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select * from ".$this->tableName." where user_id = ?");
		$statement->bindParam(1,$userId, \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		return (isset($result[0]) == 0 ? false : $result[0]);
	}

	public function updateUser($userId, $data)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("update ".$this->tableName." set email = ?,password = ?,mob_number = ?,name = ?,address = ? where user_id = ?");

		$statement->bindParam(1,$data['email'], \PDO::PARAM_STR);
		$statement->bindParam(2, $data['password'], \PDO::PARAM_STR);
		$statement->bindParam(3,$data['mob_number'], \PDO::PARAM_STR);
		$statement->bindParam(4, $data['name'], \PDO::PARAM_STR);
		$statement->bindParam(5, $data['address'], \PDO::PARAM_STR);
		$statement->bindParam(6, $userId, \PDO::PARAM_STR);
		try{
    		$statement->execute();
    		// $this->dbHandler->commit();
    		$msg = ['Detials updated successfully.','success'];
    	} catch(\PDOException $e) {
    		$msg = [$e->getMessage(),'danger'];
    	}
		return compact('msg','status');
	}


	public function getUserFromTokenId($tokenId)
	{
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare("select * from ".$this->tableName." where remember_token = ?");
		$statement->bindParam(1,$tokenId, \PDO::PARAM_STR);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		return (isset($result[0]) == 0 ? false : $result[0]);
	}
}