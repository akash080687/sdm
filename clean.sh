pgPassword="postgres"
pgHost="localhost"
pgUser="postgres"
pgDbName="sdm"

# clean Database

PGPASSWORD=$pgPassword psql -h $pgHost -U $pgUser -d $pgDbName -c "select cleanData();"

# remove files in user Directory

rm -rf ./public/sdm/*

curl -v -u admin:geoserver -XDELETE http://localhost:8080/geoserver/rest/workspaces/sdm?recurse=true
