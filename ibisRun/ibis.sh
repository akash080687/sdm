#!/bin/bash
export $(egrep -v '^#' /var/www/html/sdm/public/.env | xargs)
ibisHome="/var/www/html/sdm/public/ibis"
bioHome=$BIO

ibisHost=$IBIS_HOST
ibisUser=$IBIS_USERNAME
ibisPass=$IBIS_PASSWORD
ibisDbName=$IBIS_DATABASE
ibisPort=$IBIS_PORT

pgHost=$PG_HOST
pgUser=$PG_USERNAME
pgPass=$PG_PASSWORD
pgDbName=$PG_DATABASE
pgIbisDb="bigdata"

tables=(birdspot_geo ebird_may2014 gbif_aves_master_geo migrantwatch_geo worldbirds_geo)
columns=(birdspot_sn ebird_sn gbif_sn m_sn wb_sn)
lat_long=(birdspot ebird gbif mw wb)
speciesList=`cat $ibisHome/../birds.csv`
counter=0
IFS=',' read -ra ADDR <<< "$speciesList"
for sn in "${ADDR[@]}";do
	i=0
	for tab in "${tables[@]}";do
		counter=$((counter+1))
		query="select ${columns[$i]} as species,${lat_long[$i]}_longitude as longitude, ${lat_long[$i]}_latitude as latitude from ${tables[$i]} where ${columns[$i]}='$sn' and (${lat_long[$i]}_longitude < 100 and ${lat_long[$i]}_longitude > 65) and (${lat_long[$i]}_latitude < 40 and  ${lat_long[$i]}_latitude > 5)"
		PGOPTIONS="--search_path=observation" PGPASSWORD=$pgPass psql -U $pgUser -h $pgHost -d $pgIbisDb -c "COPY($query) TO STDOUT WITH CSV HEADER" > $counter.csv
		len=`wc -l $counter.csv`
		if [ ! "$len" = "1 $counter.csv" ]; then
			spName=`cat $counter.csv | sed -n 2p | awk -F',' '{print $1}' | sed -e 's/ //g'`
			mkdir -p $ibisHome/$spName/$tab/fileUploaded/supporting
			mkdir -p $ibisHome/$spName/$tab/result
			cp $counter.csv $ibisHome/$spName/$tab/fileUploaded/$spName.csv
			rm $counter.csv
			cp $bioHome/slope_isr.asc $bioHome/aspect_isr.asc $bioHome/alt_isr.asc $ibisHome/$spName/$tab/fileUploaded/supporting
			mId=1
			output_path=$ibisHome/$spName/$tab/result
			project_name=$sn
			level='expert'
			batchId=`PGOPTIONS="--search_path=public" PGPASSWORD=$ibisPass psql -p $ibisPort -U $ibisUser -h $ibisHost -d $ibisDbName -c "insert into ibis_batch_master (m_id, output_path, project_name, level, source) values($mId, '$output_path', '$project_name', '$level', '$tab') RETURNING id"`
			batchId=`echo $batchId | sed -e 's/id -*//g' | sed -e 's/(1 row) INSERT 0 1//g'`
			batchId=$(echo $batchId | tr -d ' ')
			maxentCmd="/bin/bash /home/sdm/maxent/ibisMaxent.sh $ibisHome/$spName/$tab/fileUploaded/$spName.csv $ibisHome/$spName/$tab/result $ibisHome/$spName/$tab/result /home/sdm/maxent $bioHome /opt/tomcat/webapps/geoserver/data/data 127.0.0.1 admin:geoserver $batchId 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19 expert "$tab"_sdm"
			PGOPTIONS="--search_path=public" PGPASSWORD=$ibisPass psql -p $ibisPort -U $ibisUser -h $ibisHost -d $ibisDbName -c "update ibis_batch_master set scriptcommand='$maxentCmd' where id=$batchId"
			eval $maxentCmd
		else
			PGPASSWORD=$ibisPass psql -p $ibisPort -U $ibisUser -h $ibisHost -d $ibisDbName -c "insert into ibis_batch_master (m_id, process, status, output_path, project_name, level, source) values ('1', '-2', 'Error - no occurence data',null, '$sn', 'expert', '$tab')"
			rm -rf $counter.csv
		fi
		i=$((i+1))
	done
	counter=$((counter+1))
	query="select a.* from (select birdspot_sn as species, birdspot_longitude as longitude, birdspot_latitude as latitude from birdspot_geo where birdspot_sn='$sn' union all select wb_sn as species, wb_longitude as longitude, wb_latitude as latitude from worldbirds_geo where wb_sn='$sn' union all select gbif_sn as species, gbif_longitude as longitude, gbif_latitude as latitude from gbif_aves_master_geo where gbif_sn='$sn' union all select m_sn as species, mw_longitude as longitude, mw_latitude as latitude from migrantwatch_geo where m_sn='$sn' union all select ebird_sn as species, ebird_longitude as longitude, ebird_latitude as latitude from ebird_may2014 where ebird_sn='$sn' ) as a where (a.longitude < 100 and a.longitude > 65) and (a.latitude < 40 and  a.latitude > 5)"
	PGOPTIONS="--search_path=observation" PGPASSWORD=$pgPass psql -U $pgUser -h $pgHost -d $pgIbisDb -c "COPY($query) TO STDOUT WITH CSV HEADER" > $counter.csv
	len=`wc -l $counter.csv`
	if [ ! "$len" = "1 $counter.csv" ]; then
		mkdir -p $ibisHome/$spName/all/fileUploaded/supporting
		mkdir -p $ibisHome/$spName/all/result
		cp $counter.csv $ibisHome/$spName/all/fileUploaded/$spName.csv
		rm $counter.csv
		cp $bioHome/slope_isr.asc $bioHome/aspect_isr.asc $bioHome/alt_isr.asc $ibisHome/$spName/all/fileUploaded/supporting
		mId=1
		output_path=$ibisHome/$spName/all/result
		project_name=$sn
		level='expert'
		batchId=`PGOPTIONS="--search_path=public" PGPASSWORD=$ibisPass psql -p $ibisPort -U $ibisUser -h $ibisHost -d $ibisDbName -c "insert into ibis_batch_master (m_id, output_path, project_name, level, source) values($mId, '$output_path', '$project_name', '$level', 'all') RETURNING id"`
		batchId=`echo $batchId | sed -e 's/id -* //g' | sed -e 's/(1 row) INSERT 0 1//g'`
		batchId=$(echo $batchId | tr -d ' ')
		maxentCmd="/bin/bash /home/sdm/maxent/ibisMaxent.sh $ibisHome/$spName/all/fileUploaded/$spName.csv $ibisHome/$spName/all/result $ibisHome/$spName/all/result /home/sdm/maxent $bioHome /opt/tomcat/webapps/geoserver/data/data 127.0.0.1 admin:geoserver $batchId 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19 expert all_sdm"
		PGOPTIONS="--search_path=public" PGPASSWORD=$ibisPass psql -p $ibisPort -U $ibisUser -h $ibisHost -d $ibisDbName -c "update ibis_batch_master set scriptcommand='$maxentCmd' where id=$batchId"
		eval $maxentCmd
	else
		PGPASSWORD=$ibisPass psql -p $ibisPort -U $ibisUser -h $ibisHost -d $ibisDbName -c "insert into ibis_batch_master (m_id, process, status, output_path, project_name, level, source) values ('1', '-2', 'Error - no occurence data',null, '$sn', 'expert', 'all')"
		rm -rf $counter.csv
	fi
done
