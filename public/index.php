<?php
require "../vendor/autoload.php";
use SDM\App\Core\App;

ini_set('display_errors',1);
error_reporting(E_ALL);
if(!isset($_SESSION))
{
	session_start();
}

/**
* Initaites the app call for SDM
*/
$app = new App();

/**
* Call the route included
*/
$app->call();