$(document).ready(function() {
  $('#submitBtn').submit(function(event){
    alert(1);
    event.preventDefault();
  });
  
  $.ajax({
      url : "/ModelConfig/getBaseUrl",
      async: false,
      success : function(response)
      {
        baseURLServ = response;
      }
  });
  lazyBindings();
  $('#clipExportModal .progress').hide();
  $('[data-toggle="popover"]').popover();
  $('#loader').hide();
  $("#settings").hide();
  $("#layerSetting").hide();

  var store = "sdm";
  baseurl = baseURLServ+store+"/wms";
  
  // $('#sdmForm').hide();
  $('#projectsList').hide();
  $('#supportingLayers').hide();
  $('#userClipsList').hide();
  $('#runNewSdm').click(function() {
      $("#sdmFormModal").modal();
      $('#supportingLayers').hide();
  	  $('#projectsList').hide();
      $('#userClipsList').hide();
  });

  $('#projects').click(function(){
  	$('#userClipsList').hide();
    $('#supportingLayers').hide();
  	$('#projectsList').slideToggle();
  });

  $('#layers').click(function(){
    $('#userClipsList').hide();
    $('#projectsList').hide();
    $('#supportingLayers').slideToggle();
  });

  $('#snapList').click(function(){
    $('#projectsList').hide();
    $('#supportingLayers').hide();
    $('#userClipsList').slideToggle();
  });

  $(".load_map").click(function(){
    layername = $(this).attr('data-attr');
    store = 'sdm';
    project = $(this).attr('data-project');
    if(layername!="")
    {
      layerId = layername;
      if(loadLayer(layername, store, baseurl, layerId) !== false)
      {
        generateUICard(layername, project);  
      } 
    }
  });

  $('.showError').click(function(){
      val = $(this).attr('data-attr');
      $.ajax({
          type : "POST",
          url : "/BatchManager/errorStatus",
          data : {
            "projName":val
          },
          success : function(response)
          {
              $.jGrowl(response,{ header: "Error!", theme:'error', position: "bottom-left" });
          }
      });
  });

  $('#batch_name').on('keyup',function(){
    val = $('#batch_name').val();
    $.ajax({
      type : "POST",
      url : "/Sdm/projectNameExists",
      data : {
        "name":val
      },
      success : function(response)
      {
        if(response == 1)
        {
          $.jGrowl('Project name already exists.',{ header: "Error!", position: "bottom-left" , theme:'error'});
          $('#submitBtn').attr('disabled',true);
        }else{
          $('#submitBtn').attr('disabled',false);
        }
      }
    });
  });

  $("#settings").on('click',function(){
    $("#layerSetting").toggle();
  });

  $('#environmental').hide();
  $('.subCategroy').hide();
  
  $('.category').click(function(){
    $('.category').removeClass('active');
    $(this).addClass('active');
    
    $('.subCategroy').slideDown();
    catId = $(this).attr('data-attr');
    $.ajax({
      url : "/LayerManager/getSubCategories",
      data : "catId="+catId,
      type : "POST",
      success : function(response)
      {
         $('.subList').html(response);
      }
    });
  });

  $('.delete_map').on('click',function(){
    layername = $(this).attr('data-attr');
    projName = $(this).attr('data-project');
    $.ajax({
        type : "POST",
        url : '/BatchManager/deleteProject',
        data : "layername="+layername+"&projName="+projName,
        success : function(response)
        {
            if(response == "")
            {
               window.location.reload(false);
            }else{
              $.jGrowl("Error occured on delete.");
            }
        }
    });
  });

  $("#registerForm").validate({
      rules: {
        name : "required",
        email : "required",
        password : {
          minlength: 5,
          required : true
        },
        re_password : {
          required: true,
          minlength: 5,
          equalTo: "#password"
        },
        mob_number : "required",
      },
      messages: {
        name : "Please enter your name",
        email : "Please enter your email",
        password : {
          required : "Please enter valid password",
          minlength : "Password must be minimum 5 characters long."

        },
        re_password : {
          required : "Please re type password",
          equalTo : "Password must match"
        },
        mob_number : "Please enter valid mobile number" 
      }
  });

  $("#layerSetting").draggable({containment: "body", scroll: false});

  $('.rerun').on('click',function(){
    projName = $(this).attr('data-project');
      $.ajax({
          url : "/Sdm/rerun",
          data : "project="+projName,
          type : "POST",
          success : function(response){
              if(response)
              {
                window.location.reload(false);
              }
          }
      });
  });

  $('#printMap').on('click',function(){
    $("#clipExportModal").modal();
    $('#supportingLayers').hide();
    $('#projectsList').hide();
    $('#userClipsList').hide();
  });

  $('#clipExportForm').submit(function(e){
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
      type: 'POST',
      url: '/Sdm/clipExport',
      data : formData,
      processData: false,
      contentType: false,
      success: function(response){
        var json_obj = $.parseJSON(response);
        if(json_obj.success)
        {
          pathToCheck = json_obj.path;
          $('#clipExportForm .btn').attr('disabled',true);
          $('#clipExportModal .progress').show();
          var fileCheck = setInterval(
            function(){
              $.ajax({
                url : "/Sdm/checkClipDone",
                data : "url="+pathToCheck,
                type: "POST",
                success: function(response)
                {
                  if(response)
                  {
                    clearInterval(fileCheck);
                    $('#clipExportForm .btn').attr('disabled',false);
                    $('#clipExportModal .progress').hide();
                    $.jGrowl("Clipping done successfully",{ header: "Success !!", theme:'error', position: "bottom-left" });
                  }
                }
              });
            }
          ,2000);
        }
         
      }
    });
  });
});

function generateUICard(layername, project)
{
    layername = layername.replace(/ /g, '__');
    project = project.replace(/ /g, '__');
    uiElem = "<div class='card'  id='"+layername+"'>\
    <div class='card-header'>\
      <h5 class='mb-0'>\
        <button class='btn btn-link' data-toggle='collapse' data-target='#"+layername+"collapse' aria-expanded='true' aria-controls='"+layername+"collapse''>\
          "+project+"\
        </button>\
        <a title='Remove layer from view' style='float:right;' class='link removeLayer' data-attr='"+layername+"'>\
          <i class='fa fa-trash fa-md'></i>\
        </a>\
      </h5>\
    </div>\
    <div id='"+layername+"collapse' class='collapse' aria-labelledby='"+layername+"' data-parent='#accordion'>\
      <div class='card-body'>\
      <div class='form-group row'>\
            <label class='control-label col-md-3'>Opacity :</label>\
            <div class='col-md-8'>\
               <input id='"+layername+"_opacity' type='range' name='"+layername+"_opacity' min='0' max='1' step='0.01' onchange='changeLayerOpacity(\""+layername+"\")'>\
            </div>\
      </div>\
      <div class='form-group row featureInfo'> \
          <label class='control-label col-md-3'>Feature Info :</label>\
            <div class='featureInfoContent col-md-12'>\
            Click on map to load.\
            </div>\
      </div>\
      </div>\
    </div>\
    </div>";
    $('#accordion').append(uiElem);
    maxWidth = $("#layerSetting").width()-6;
    $('#'+layername+' .card-body').resizable({ maxWidth: maxWidth, minWidth: 450 }).on('resize', function (e) {
      e.stopPropagation();
    });
}

function updateUICard(layername)
{
  $('#'+layername).remove();
}

function snapShot() {
	map.once('postcompose', function(event) {
	  	var canvas = event.context.canvas;
		if (navigator.msSaveBlob) {
		  navigator.msSaveBlob(canvas.msToBlob(), 'map.png');
		} else {
		  canvas.toBlob(function(blob) {
		    saveAs(blob, 'map.png');
		  });
		}
	});
	map.renderSync();
}

function lazyBindings()
{
    $(document).on('click','.removeLayer',function(event){
      layername = $(this).attr('data-attr');
      if(layername!="")
      {
        removeLayer(layername);
        updateUICard(layername);
      }
    });

    $(document).on('click','.subcatlist li',function(event){  
      $('.subcatlist li').removeClass('active');
      $(this).addClass('active');
      subCatId = $(this).attr('data-attr');
      $.ajax({
          url : "/LayerManager/getLayerList",
          data : "subCatId="+subCatId,
          type : "POST",
          success : function(response)
          {
            $('.layerList').html(response);
          }
      });
    });

    $(document).on('click','.layerList li .viewLayer',function(e){
        workspace = $(this).parent().attr('data-workspace');
        store = $(this).parent().attr('data-store');
        layer = $(this).parent().attr('data-layer');
        from = "dp";
        baseurl = "http://db2.biota.in:8080/geoserver/"+workspace+"/wms";
        if(loadLayer(layer, store, baseurl, layer, from) !== false)
        {
          generateUICard(layer, layer);  
        } 
    });

    $(document).on('click','#addMore',function(){
      $(this).parent().append('<input name="supporting[]" type="file">');
    });

    $('#expert').click(function(){
        $(this).parent().next().toggleClass('hide');
        valBtn = $('#levelVal').val();
        (valBtn == "true") ? $('#levelVal').val("false") : $('#levelVal').val("true");
        valTxt  = (valBtn == "true") ? 'Add new project (beginner)' : 'Add new project (advanced)';
        $(this).parent().parent().parent().parent().find('.modal-title').html(valTxt);
    });

    $('#export').click(function(){
      resp = "";
      $.each(layerList, function(i){
        center = ol.proj.transform(map.getView().getCenter(), 'EPSG:3857', 'EPSG:4326');
        name = layerList[i].get('Name');
        store = layerList[i].From == 'sdm' ? 'sdm' : storeList[i];
        domainName = layerList[i].From == 'sdm' ? baseURLServ : 'http://db2.biota.in:8080/geoserver/';
        layerWMSUrl = domainName+store+"/wms";
        resp = printMap(store, i, layerWMSUrl, center);
      });
      url = domainName+"pdf/print.pdf?"+"spec="+encodeURIComponent(JSON.stringify(resp[0]));
      window.location = url;
    });
    $.ajax({
      url : "/Sdm/findIbis",
      type : "GET",
      success : function(response)
      {
        ibisList = $.parseJSON(response);
        $( "#ibisSearch" ).autocomplete({
          source: Object.keys(ibisList),
          select: function (event, ui)
          {
            if(ibisLoaded != "")
            {
              removeLayer(ibisLoaded);
              updateUICard(ibisLoaded);
            }
            layer = ibisList[ui.item.label];
            ibisLoaded = layer;
            if(loadLayer(layer, "sdm", baseurl, layer) !== false)
            {
              generateUICard(layer, layer);
            } 
          }
        });
      }
    });
}
