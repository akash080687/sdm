/**
* Global variables to be used throughtout JS
*/
/***************************************************************/
/***************************************************************/
var state_layer, district_layer, tehsil_layer, village_layer;
var legendContent = $("#legendContent").length ? $("#legendContent") : "";
var loader = $('#loader').length ? $('#loader') : "";
var errorECSubmit = $("#errorECSubmit").length ? $("#errorECSubmit") : "";
var layerRow = $("#layerRow").length ? $("#layerRow") : "";
var legendRow = $("#legendRow").length ? $("#legendRow") : "";
var layerOpacitySlider = $("#layerOpacitySlider").length ? $("#layerOpacitySlider") : "";
var mapOnClickKey;
var layerName;
var layersByName = {};
var ibisList;
var ibisLoaded = "";
// OSM org version
// var osmLayer = new ol.layer.Tile({
//     source: new ol.source.OSM()
// });

// OSM india version
var osmLayer =	new ol.layer.Tile({
  source: new ol.source.XYZ({
    url: 'https://{a-c}.tiles.mapbox.com/v4/openstreetmap.1b68f018/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoiamluYWxmb2ZsaWEiLCJhIjoiY2psejFtZG8wMWhnMjNwcGFqdTNjaGF2MCJ9.ZQVAZAw8Xtg4H2YSuG4PlA',
    crossOrigin: 'anonymous'
  })
});

var baseurl;
var baseURLServ;
var googleLayer = new olgm.layer.Google();

var mousePositionControl = new ol.control.MousePosition({
	coordinateFormat: ol.coordinate.createStringXY(4),
	projection: 'EPSG:4326',
	// comment the following two lines to have the mouse position
	// be placed within the map.
	className: 'custom-mouse-position',
	target: document.getElementById('latlonInfo'),
	undefinedHTML: '&nbsp;'
});
var scaleLine = new ol.control.ScaleLine({
	minWidth: 100
});

var map = new ol.Map({
    target: 'map',
    view : new ol.View({
		center:  ol.proj.fromLonLat([79,22]),
		zoom: 4.5
	})
});
// map.addLayer(googleLayer);
map.addLayer(osmLayer);
map.addControl(mousePositionControl);
map.addControl(scaleLine);
var olGM = new olgm.OLGoogleMaps({map: map});
olGM.activate();
var layerList = {};
var layerFilters = {};
var storeList = {};
var layerNameList = {};
/***************************************************************/
/***************************************************************/


/** Global Load layer function **/

function loadLayer(layerName, store, baseurl, layerId, from="sdm")
{
	// If settings is not visible
	if( ! $("#settings").is(":visible") )
	{
		$("#settings").show();	
	}
	
	// Dont add if layer already added
	if(layerList[layerId] !== undefined)
	{
		$.jGrowl('Already added.',{life:4000, theme:'error-fixht'});
		return false;
	}
	// Dont add if layer already added

	loader.show();
	layerFilters[layerId] = {};
	source = new ol.source.TileWMS({
        params : {
            'layers' : store+":"+layerName,
	    'version': '1.1.0'
        },
        url : baseurl,
        crossOrigin: 'anonymous'
    });
	layerList[layerId] = new ol.layer.Tile({
        source : source
    });
    storeList[layerId] = store;
    layerNameList[layerId] = layerName;
    // Show loader
	var tile_loading = 0, tile_loaded = 0;
	source.on('tileloadstart', function() {
		++tile_loading;
	});
	
	source.on('tileloadend', function() {
		++tile_loaded;
		if(tile_loaded == tile_loading){
	        loader.hide();
	    }
	});
	// Show loader
    layerList[layerId].set('Name', layerName);
    layerList[layerId]['From'] = from;
    map.addLayer(layerList[layerId]);
    getBounds(layerList[layerId],baseurl);
    layersByName[layerName] = layerList[layerId];
//  temporal layer selection 
//  layerList['MOD13Q1_006_NDVI'].getSource().updateParams({'TIME': '2005-01-03'});
    legendContent.show();
    mapOnClickKey != undefined ? map.un('singleclick', getFeatureInfo) : "";
    mapOnClickKey = map.on('singleclick', getFeatureInfo);
    legendHTML = "<div class='legendLayer "+layerId+"'>\
    				<div class='title'>"+layerName+"</div>\
    				<div class='imgLegend'>\
						<img src='"+baseurl+"?TRANSPARENT=true&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/gif&EXCEPTIONS=application/vnd.ogc.se_xml&LAYER="+store+":"+layerName+"'/>\
					</div>\
				</div>";
    legendContent.append(legendHTML);
}

function getFeatureInfo(evt) {
	getXy = evt.pixel;
	bbox = map.getView().calculateExtent(map.getSize());
	var view = map.getView();
    var viewResolution = (view.getResolution());
    Object.keys(layerList).forEach(function (layer) {
		if(typeof layerList[layer] != "undefined"){
			from = layerList[layer]['From'];
			if (layerList[layer].getVisible() && layerList[layer].get('Name')!='Basemap') {
				if(from == "dp")
				{
					url = "http://db2.biota.in:8080/geoserver/"+storeList[layer]+"/wms?LAYERS="+storeList[layer]+"%3A"+layerNameList[layer]+"&QUERY_LAYERS="+storeList[layer]+"%3A"+layerNameList[layer]+"&STYLES=&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&BBOX="+bbox+"&FEATURE_COUNT=10&HEIGHT="+$(window).height()+"&WIDTH="+$(window).width()+"&FORMAT=image%2Fpng&INFO_FORMAT=text%2Fhtml&SRS=EPSG%3A900913"
				}else{
					url = baseURLServ+storeList[layer]+"/wms?LAYERS="+storeList[layer]+"%3A"+layerNameList[layer]+"&QUERY_LAYERS="+storeList[layer]+"%3A"+layerNameList[layer]+"&STYLES=&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetFeatureInfo&BBOX="+bbox+"&FEATURE_COUNT=10&HEIGHT="+$(window).height()+"&WIDTH="+$(window).width()+"&FORMAT=image%2Fpng&INFO_FORMAT=text%2Fhtml&SRS=EPSG%3A900913";
				}
				if (url) 
				{
					$('#loader').show();
					$.ajax({
						url: url+"&X="+getXy[0]+"&Y="+getXy[1],
						type: 'get',
						success: function(response){
							layerElemId = layer.replace(/ /g, '__');
							featureInfoContent = $('#'+layerElemId+'collapse .featureInfo div');
							var table = response.split('<body>').pop().split('</body>').shift();
							featureInfoContent.html(table);
							$('#'+layerElemId+'collapse .featureInfo div table').addClass('table table-dark');
							// "http://db2.biota.in:8080/geoserver/biophy/wms?TRANSPARENT=true&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&FORMAT=image/gif&EXCEPTIONS=application/vnd.ogc.se_xml&LAYER=biophy:ind_foresttype&SCALE=55467893.20400156"
							// Static changes for SDM featureInfo
							// if(from == "sdm")
							// {
							// 	$('#'+layerElemId+'collapse .featureInfo table tr th:nth-child(1)').hide();
							// 	$('#'+layerElemId+'collapse .featureInfo table tr th:nth-child(2)').hide();
							// 	$('#'+layerElemId+'collapse .featureInfo table tr td:nth-child(1)').hide();
							// 	$('#'+layerElemId+'collapse .featureInfo table tr td:nth-child(2)').hide();
							// 	$('#'+layerElemId+'collapse .featureInfo table tr th:nth-child(3)').html('Probability');
							// }
							// Static changes for SDM featureInfo
							$('#loader').hide();
						},
						error: function(e){
							layerElemId = layer.replace(/ /g, '__');
							featureInfoContent = $('#'+layerElemId+'collapse .featureInfo div');
							featureInfoContent.html("Error: coudn't fetch details. Please contact site administrator.");
						}
					});
					$('#loader').hide();
				}
			}
		}
	});
}

/** Global getBounds function **/


function getBounds(layer, baseurl)
{
	var url = baseurl+'?request=GetCapabilities&service=WMS&version=1.1.1';
    var parser = new ol.format.WMSCapabilities();
	$.ajax(url).then(function (response) {
    
        var result = parser.read(response);
        var Layers = result.Capability.Layer.Layer;
        var extent;
        for (var i = 0, len = Layers.length; i < len; i++) {
            var layerobj = Layers[i];
            if (layerobj.Name == layer.get('Name'))
            {
            	extent = layerobj.BoundingBox[0].extent;
            	extent = ol.proj.transformExtent(extent, ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857'));
            	map.getView().fit(extent, map.getSize());
            }
        }
    });
}

function updateCQL(type, code){
	for (var lyr in layerList) {
		layerList[lyr].getSource().updateParams({'cql_filter': type+'_code=' + code + ';sc_id='+ layerFilters[lyr]['cql_filter_sc_id'] + layerFilters[lyr]['cql_filter_field']}); 
	}  
}



/** Global remove layer function **/


function removeLayer(layerId)
{
	layerId = layerId.replace(/__/g, ' ');
	map.removeLayer(layerList[layerId]);
	delete layerList[layerId];
	delete storeList[layerId];
	delete layerNameList[layerId];
	legendContent.find('.'+layerId).remove();
	// If no layer to display 
    if(Object.keys(layerList).length == 0)
    {
        $("#settings").hide();
        legendContent.hide();
        $("#layerSetting").hide();
    }
}

function changeLayerOpacity(layername){
	value = $('#'+layername+"_opacity").val();
	console.log(value);
	layerList[layername].setOpacity(Number(value));
}

function printMap(store, layer, baseurl, center)
{
	title = $("#printPdfMap input[name='mapTitle']").val();
	fileName = $("#printPdfMap input[name='outputFilename']").val();
	legend = $("#printPdfMap input[name='legendName']").val();
	dpi = $("#printPdfMap input[name='dpi']").val();
	comments = $("#printPdfMap input[name='comments']").val();
	scale = $("#scaleAttr option:selected").val();
	jsonObj = [];
	item = {};
	item['comment'] = comments;
	item['mapTitle'] = title;
	item['layout'] = "A4 portrait";
	item["srs"]="EPSG:4326";
	item["units"]="degrees";
	item["geodetic"]="false";
	item["outputFilename"]=fileName;
	item["outputFormat"]="pdf";
	item["layers"] = [{
		"type": "WMS",
		"layers": [store+":"+layer],
		"baseURL": baseurl,
		"format": "image/jpeg",
		"opacity": "1"
	}];
	item["pages"] = [{
		"center": center,
		"scale": scale,
		"dpi": dpi,
		"geodetic": "false"
	}];
	item["legends"] = [{
		"classes": [{
			"icons": [baseurl+"?version=1.3.0&TRANSPARENT=TRUE&SERVICE=WMS&REQUEST=GetLegendGraphic&EXCEPTIONS=application/vnd.ogc.se_xml&LAYER="+store+":"+layer+"&FORMAT=image/png"],
			"name": "",
			"iconBeforeName": "true"
		}],
		"name": legend
	}];
	jsonObj.push(item);
	return jsonObj;
}
