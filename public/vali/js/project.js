$(document).ready(function(){
  $('#projectsList tr').each(function(i) {
    status = $(this).find('td:nth-child(3) div.status').text();
    if(status == "In queue" || status == "Running")
    {
        parent = $(this).find('td:nth-child(3) div.status');
        elem = $(this).find('td:nth-child(2)').text();
        var refreshIntervalId = setInterval(function(){
           $.ajax({
              url : "/BatchManager/processStatus",
              data : "projName="+ elem,
              success : function(response)
              {
                if(response != "")
                {
                    response = $.parseJSON(response);

                    if(response.process == "-1")
                    {
                        parent.html(response.status);
                        parent.parent().find('.progress-bar').css('width','0%');
                        clearInterval(refreshIntervalId);
                    }else{
                        parent.html(response.status);
                        parent.parent().find('.progress-bar').css('width',response.process+'%');
                        parent.parent().find('.progress-bar').html(response.process+'%');
                        if(response.status == "Running")
                        {
                           parent.parent().parent().find('.delete_map').hide();
                        }
                        if(response.process == "100")
                        {
                          $.jGrowl("Modelling for "+elem+" is completed. Reload the page to generate view links.",{ position: "bottom-left", sticky : true, theme : 'success'});
                          parent.parent().find('.progress').hide();
                          clearInterval(refreshIntervalId);
                        }
                    }
                }
              }
           });
        },2000);
        return false; 
    }
  });
});